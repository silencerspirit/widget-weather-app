export default {
  ru: {
    btnText: "Получить",
    fieldLabel: "Введите город",
    requiredRule: "Поле обязательно для ввода",
    emptyCityRule: "Такого города не существует",
    availableCityRule: "Этот город уже добавлен",
  },
  en: {
    btnText: "Find",
    fieldLabel: "Enter the city",
    requiredRule: "The field is required for entering",
    emptyCityRule: "There is no such city",
    availableCityRule: "This city has already been added",
  },
};
