export default {
  ru: {
    temperatureFeelsLike: "Температура ощущается",
    pressure: "Давление",
    humidity: "Влажность",
    windSpeed: "Скорость ветра",
    meterPerSecond: "м/сек",
    windDirection: "Направление ветра",
    windGusts: "Порывы ветра",
    sunrise: "Восход",
    sunset: "Закат",
  },
  en: {
    temperatureFeelsLike: "The temperature is felt",
    pressure: "Pressure",
    humidity: "Humidity",
    windSpeed: "Wind speed",
    meterPerSecond: "m/sec",
    windDirection: "Wind direction",
    windGusts: "Wind gusts",
    sunrise: "Sunrise",
    sunset: "Sunset",
  },
};
